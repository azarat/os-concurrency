#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * A function to search a file for a substring.  The file F has to be
 * opened and the function is going to call subsequent fgetc on F
 * until it reaches EOF or finds the match.
 * @param 	haystack 	File pointer: Input file, already opened for reading
 * @param 	needle 		Char array: Pattern to be found.
 * @return 	-1 			Pattern was not found
 * @return 	x > 0 		Where x is the offset of the match.
 */
int search (FILE *haystack, const char *needle) {
    int positionInNeedle, backwardsPositionInNeedle, currentChar, offset = 1;
    const char *nextChar;

	// Loop through all characters from haystack until EOF is reached
    while ( EOF != (currentChar = fgetc (haystack)) ) {
		// Increment counter to keep track of offset
		offset++;
		// Skip unwanted first characters
        if (currentChar != *needle) {
            continue;
		}
		
        for (positionInNeedle = 1, nextChar = needle + 1;		*nextChar != '\0';		positionInNeedle++, nextChar++) {
            if (*nextChar != (currentChar = fgetc (haystack))) {
                ungetc (currentChar, haystack);
                break;
            }
		}
		
        if (*nextChar == '\0') {
            return offset;
        } else {
            for (backwardsPositionInNeedle = positionInNeedle - 1; 		backwardsPositionInNeedle > 0; 		backwardsPositionInNeedle--) {
				ungetc (needle[backwardsPositionInNeedle], haystack);
			}
		}
    }

    return -1;
}
	
/**
 * Main method. Accepts multiple argv inputs, 1 for the string to search for and all others for filenames to open and search in.
 * @return	EXIT_SUCCESS	Successfully searched for string with no errors. Does not imply string was found.
 * @return	EXIT_FAILURE	Failed: Incorrect usage.
 */
int main (int argc, char *argv[]) {
	// Incorrect input, print usage
    if (argc < 2) {
    	printf( "usage: %s string-to-search list-of-files \n", argv[0] );
		exit(EXIT_FAILURE);
    }
	// Loop through input filenames
    int inputCount;
    for (inputCount = 2; inputCount < argc; inputCount++) {
		// Open input file to search, read-only
		FILE *file = fopen( argv[inputCount], "r" );
        // fopen returns 0, the NULL pointer, on failure - print error
        if ( file == 0 ) {
            printf( "Could not open file \"%s\"\n", argv[inputCount] );
        } else {
			// Search file for input string
			int offset = search(file, argv[1]);
            if (offset != -1) {
				// Woo! Found a match!
				printf ("pattern \"%s\" found in file \"%s\", offset = %d \n", argv[1], argv[inputCount], offset);
            }
		}
    }
	exit(EXIT_SUCCESS);
}