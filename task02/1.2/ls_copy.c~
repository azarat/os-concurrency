#include <stdio.h>   /* printf, stderr, fprintf */
#include <stdlib.h>  /* exit */
#include <string.h> /* memset */
#include <unistd.h>  /* _exit, fork */
#include <errno.h>   /* errno */
#include <sys/stat.h> /* fstat, file stats */
#include <sys/types.h> /* pid_t */

#define READ 0
#define WRITE 1

char* loadFd(int fd); 
int count(char* buf);

int main(int argc, char* argv[])
{
	char* buf;
	pid_t process_id = 0;

	// Create pipe for communication between parent and child process
	int fd[2];
	pipe(fd);
	
	// Create fork. All output will be written to stdout as both run simultaneously
	process_id = fork();

	if (process_id == -1) {
		// When fork() returns -1, an error occurred (for example, number of processes reached the limit).
		fprintf(stderr, "fork failed, error %d\n", errno);
		exit(EXIT_FAILURE);
	} else if (process_id == 0) {
		// When fork() returns 0, we are in the child process. 
		// Close pipe input
		close(fd[READ]); 
		// Close stdout
		close(WRITE); 
		// Redirect stdout to pipe output, return error if failed
		if (dup2( fd[WRITE] , WRITE ) < READ) {
			fprintf(stderr, "dup2 in child failed, error %d\n", errno);
			exit(EXIT_FAILURE);
		}
		// Call ls
		execv("/bin/ls", argv); 
	} else {
		/* When fork() returns a positive number, we are in the parent process
		* (the fork return value is the PID of the newly created child process) */
		
		// Wait for child to finish
		wait(&process_id);
		// Close pipe output
		close(fd[WRITE]);
		// Close stdin
		close(READ); 
		
		// Read the output from the child
		buf = loadFd( fd[READ] );
		
		// Print normally if there are a maximum of 25 files
		if ( count(buf) <= 25 ) {
			printf("%s", buf);
		} else {
			// Use popen2 function (below) to fork less with two pipes (one for input, one for output)
			int infp, outfp;
			if (popen2("/usr/bin/less", &infp, &outfp) <= 0) {
				fprintf(stderr, "less in parent failed, error %d\n", errno);
				exit(EXIT_FAILURE);
			}
			
			// Allocate space to store output from less - assume it won't be longer than buf*2
			char* lessbuf = (char*) malloc( sizeof(buf)*2 );
			// Write output from child process (ls) to new child process (less)
			write(infp, buf, strlen(buf) );
			// Close less input pipe
			close(infp);
			// Read output from less
			read(outfp, lessbuf, strlen(buf)*2 );
			// Print output from less
			printf("%s", lessbuf);
		}
		
		exit(0); /* End this poor soul's troubled life */
	}
}

char* loadFd(int fd) 
{
        char* buf = NULL;
        char* bufp;
        size_t bufsz, cursz, curpos;

        ssize_t ssz;
        struct stat st;

        if(fstat(fd, &st) >= 0)
		{
                bufsz = (size_t) st.st_blksize;
        } 
		else 
		{
                
        }

        buf = (char*) malloc(bufsz);

        curpos = 0;
        cursz = bufsz;

        while((ssz = read(fd, buf + curpos, bufsz)) > 0)
		{
                curpos += ssz;
                cursz = curpos + bufsz;
                if(NULL == (bufp = (char*) realloc(buf, cursz)))
				{
                        
                }
                buf = bufp;
        }
        buf[curpos] = '\0';
        return buf;
}

int count(char* buf)
{
	char current = buf[0];
	int count = 0;
	int i = 0;
	while (current != '\0')
	{
		current = buf[i];
		if (current == '\n')
		{
			count++;
		}
		i++;
	}
	return count;
}

// Make it easier to read AND write to a forked process - popen is one way.
pid_t popen2(const char *command, int *infp, int *outfp) {
	int p_stdin[2], p_stdout[2];
	pid_t pid;

	if (pipe(p_stdin) != 0 || pipe(p_stdout) != 0)
	return -1;

	pid = fork();
	if (pid < 0)
		return pid;
	else if (pid == 0) {
		close(p_stdin[WRITE]);
		dup2(p_stdin[READ], READ);
		close(p_stdout[READ]);
		dup2(p_stdout[WRITE], WRITE);
		execl("/bin/sh", "sh", "-c", command, NULL);
		perror("execl");
		exit(1);
	}

	if (infp == NULL)
		close(p_stdin[WRITE]);
	else
		*infp = p_stdin[WRITE];
	if (outfp == NULL)
		close(p_stdout[READ]);
	else
		*outfp = p_stdout[READ];
	return pid;
}