#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <time.h>
#include <errno.h>   /* errno */
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/time.h>

#include <pthread.h>

/* Dummy function spawned by every thread/process.  */
int dummy (void *data)
{
	return 0;
}

/* Time difference between a and b in microseconds.  */
int64_t xelapsed (struct timespec a, struct timespec b)
{
	return ((int64_t)a.tv_sec - b.tv_sec) * 1000000
		+ ((int64_t)a.tv_nsec - b.tv_nsec) / 1000LL;
}

/* Measure the time for NUMBER fork creations.  */
void measure_forks (unsigned number)
{
	struct timespec start, stop, finish;
	unsigned i = 0;
	pid_t processes[number];

	clock_gettime(CLOCK_REALTIME, &start);
	// loop over n
	for (i = 0;i < number; i++) {
	// create a new process and save the id
		processes[i] = fork();
	// When fork() returns -1, an error occurred (for example, number of processes reached the limit).
		if (processes[i] == -1) {
			fprintf(stderr, "fork failed, error %d\n", errno);
			exit(EXIT_FAILURE);
		} 
	// When fork() returns 0, we are in the child process. 
		else if (processes[i] == 0) {
		// call dummy and make sure that the child exists
			dummy(NULL);
			exit(EXIT_SUCCESS);
		}
	}
	clock_gettime(CLOCK_REALTIME, &stop);
	// wait for all the processes
	for (i = 0; i < number; i++) {
		wait(&processes[i]);		
	}

	clock_gettime(CLOCK_REALTIME, &finish);
	// print out the results
	printf ("process: num=%03u, fork=%03li, wait=%03li, totatl=%03li\n",
		number, xelapsed (stop, start), xelapsed (finish, stop),
		xelapsed (finish, start));
}

/* Measure the time for NUMBER thread creations.  */
void measure_threads (unsigned number)
{
	struct timespec start, stop, finish;
	unsigned i = 0;
	pthread_t threads[number];
	int rc;

	clock_gettime(CLOCK_REALTIME, &start);
	// loop over n
	for (i = 0;i < number; i++) {
	// creates a thread
		rc = pthread_create(&threads[i], NULL, dummy, (void *)i);
	// catch any errors
		if (rc){
			printf("ERROR; return code from pthread_create() is %d\n", rc);
			exit(EXIT_FAILURE);
		}
	}
	clock_gettime(CLOCK_REALTIME, &stop);
	// join all the threads
	for (i = 0; i < number; i++) {
		pthread_join( threads[i], NULL);
	}

	clock_gettime(CLOCK_REALTIME, &finish);
	// print result
	printf ("threads: num=%03u, fork=%03li, wait=%03li, totatl=%03li\n",
		number, xelapsed (stop, start), xelapsed (finish, stop),
		xelapsed (finish, start));
}

int main (int argc, char *argv[])
{
	if ( argc != 2 ) /* argc should be 2 for correct execution */
	{
		/* We print argv[0] assuming it is the program name */
		printf( "usage: %s number-of-forks \n", argv[0] );
		exit(EXIT_FAILURE);
	}
	int n = atoi(argv[1]); // get argument casting to int
	measure_forks(n); // measure forks
	measure_threads (n); // measure n threads
	return EXIT_SUCCESS; // exit
}
