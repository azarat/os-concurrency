#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <time.h>
#include <errno.h>   /* errno */
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/time.h>

#include <pthread.h>

// This makes it easier to malloc multiple string arguments 
typedef struct {char* haystackPath; char* needle;} arguments;

/**
 * A function to search a file for a substring.  The file F has to be
 * opened and the function is going to call subsequent fgetc on F
 * until it reaches EOF or finds the match.
 * @param 	haystack 	File pointer: Input file, already opened for reading
 * @param 	needle 		Char array: Pattern to be found.
 * @return 	-1 			Pattern was not found
 * @return 	x > 0 		Where x is the offset of the match.
 */
int search (FILE *haystack, const char *needle) {
    int positionInNeedle, backwardsPositionInNeedle, currentChar, offset = 1;
    const char *nextChar;

	// Loop through all characters from haystack until EOF is reached
    while ( EOF != (currentChar = fgetc (haystack)) ) {
		// Increment counter to keep track of offset
		offset++;
		// Skip unwanted first characters
        if (currentChar != *needle) {
            continue;
		}
		
        for (positionInNeedle = 1, nextChar = needle + 1;		*nextChar != '\0';		positionInNeedle++, nextChar++) {
            if (*nextChar != (currentChar = fgetc (haystack))) {
                ungetc (currentChar, haystack);
                break;
            }
		}
		
        if (*nextChar == '\0') {
            return offset;
        } else {
            for (backwardsPositionInNeedle = positionInNeedle - 1; 		backwardsPositionInNeedle > 0; 		backwardsPositionInNeedle--) {
				ungetc (needle[backwardsPositionInNeedle], haystack);
			}
		}
    }

    return -1;
}

/**
 * This is a wrapper function for the search method (called when creating a new thread).
 * Opens input file (throws an error if it fails), and calls the search function.
 * Thread returns output from the search function is returned.
 * @param 	args 	Struct (arguments): Should contain string path to file to search in, and string needle to search for
 * @return 	void* 	Pointer to output from search function.
 */
void* search_wrapper(void * inargs) {
	// Cast args pointer back to correct type after thread creation
	arguments * args = (arguments *)inargs;
    // Grab arguments froms struct
    char * haystackPath = args->haystackPath;
    char * needle = args->needle;
	// Open the file to search in
    FILE *haystack = fopen( haystackPath, "r" );
    // fopen returns 0, the NULL pointer, on failure
    if ( haystack == 0 ) {
        printf( "Could not open file: %s \n", args->haystackPath);
		return (void*)-1;
    } else {
		// Perform search
		intptr_t searchResult = search(haystack, needle); // int the same size of the pointer to avoid warnings.
		free(args); // free the space for the args
		// Exit thread, passing output from search method
        return (void*)searchResult;
    }
}

/**
 * Main method. Accepts multiple argv inputs, 1 for the string to search for and all others for filenames to open and search in.
 * Checks if the input format is correct, opens the threads, and prints the results from them.
 * @return	EXIT_SUCCESS	Successfully searched for string with no errors. Does not imply string was found.
 * @return	EXIT_FAILURE	Failed: Incorrect usage or problem creating threads.
 */
int main (int argc, char *argv[]) {
	// Array to hold all the threads
    pthread_t threads[argc-2];
	// needle: the input string to search for
    char* needle = argv[1]; 
	// Incorrect input, print usage
    if (argc < 2) {
    	printf( "usage: %s string-to-search list-of-files \n", argv[0] );
		exit(EXIT_FAILURE);
    }
	// Loop through input filenames, creating threads to search them all in parallel
    int inputCount;
    for (inputCount = 2; inputCount < argc; inputCount++) {
		// malloc the struct
        arguments * currentSearchArgs = malloc(sizeof(arguments)); 
		// Put values in the struct, ready to pass to the wrapper method
        currentSearchArgs->haystackPath = argv[inputCount];
        currentSearchArgs->needle = needle;
		// Make a thread using the search wrapper method, passing the struct as the argument
		int pthreadReturn = pthread_create(&threads[inputCount-2], NULL, search_wrapper, (void *)currentSearchArgs);
		currentSearchArgs = NULL;
        // If pthread_create failed and returned non-0, exit with error
        if (pthreadReturn) {
            printf("ERROR; return code from pthread_create() is %d\n", pthreadReturn);
            exit(EXIT_FAILURE);
        }
    }
    // Loop through the threads
    int threadCount;
	int threadResult;
	for (threadCount = 0; threadCount < argc-2; threadCount++) {
		// Join the thread and save the return value in the threadResult variable
		pthread_join(threads[threadCount], (void *)&threadResult);
		// If the needle was successfully found by this thread, print message. Otherwise do nothing	
		if (threadResult != -1) {
			printf ("pattern \"%s\" found in file \"%s\", offset = %d \n", needle, argv[threadCount+2], threadResult);
		}
    }
    // Exit with a happy message
    exit(EXIT_SUCCESS);
}