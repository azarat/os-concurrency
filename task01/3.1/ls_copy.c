#include <stdio.h>   /* printf, stderr, fprintf */
#include <stdlib.h>  /* exit */
#include <unistd.h>  /* _exit, fork */
#include <errno.h>   /* errno */
#include <sys/stat.h> /* fstat, file stats */
#include <sys/types.h> /* pid_t */

char* loadFd(int fd); 
int count(char* buf);

int main(int argc, char* argv[])
{
	char* buf;
	pid_t process_id = 0;

	// Create pipe for communication between parent and child process
	int fd[2];
	pipe(fd);
	
	// Create fork. All output will be written to stdout as both run simultaneously
	process_id = fork();

	if (process_id == -1) {
		// When fork() returns -1, an error occurred (for example, number of processes reached the limit).
		fprintf(stderr, "fork failed, error %d\n", errno);
		exit(EXIT_FAILURE);
	} else if (process_id == 0) {
		// When fork() returns 0, we are in the child process. 
		// Close pipe input
		close(fd[0]); 
		// Close stdout
		close(1); 
		// Redirect stdout to pipe output, return error if failed
		if (dup2( fd[ 1 ] , 1 ) < 0) {
			fprintf(stderr, "dup2 in child failed, error %d\n", errno);
			exit(EXIT_FAILURE);
		}
		// Call ls
		execv("/bin/ls", argv); 
	} else {
		/* When fork() returns a positive number, we are in the parent process
		* (the fork return value is the PID of the newly created child process) */
		
		// Wait for child to finish
		wait(&process_id);
		// Close pipe output
		close(fd[1]);
		// Close stdin
		close(0); 
		// Read the output from the child char by char
		buf = loadFd( fd[0] );

		// Print if there are a maximum of 25 files
		if ( count(buf) <= 25 ) {
			printf("%s", buf);
		} else {
			// Otherwise print error
			printf("Too many files. Count:%d \n", count(buf));
		}

		exit(0); /* End this poor soul's troubled life */
	}
}

char* loadFd(int fd) 
{
        char* buf = NULL;
        char* bufp;
        size_t bufsz, cursz, curpos;

        ssize_t ssz;
        struct stat st;

        if(fstat(fd, &st) >= 0)
		{
                bufsz = (size_t) st.st_blksize;
        } 
		else 
		{
                
        }

        buf = (char*) malloc(bufsz);

        curpos = 0;
        cursz = bufsz;

        while((ssz = read(fd, buf + curpos, bufsz)) > 0)
		{
                curpos += ssz;
                cursz = curpos + bufsz;
                if(NULL == (bufp = (char*) realloc(buf, cursz)))
				{
                        
                }
                buf = bufp;
        }
        buf[curpos] = '\0';
        return buf;
}

int count(char* buf)
{
	char current = buf[0];
	int count = 0;
	int i = 0;
	while (current != '\0')
	{
		current = buf[i];
		if (current == '\n')
		{
			count++;
		}
		i++;
	}
	return count;
}
