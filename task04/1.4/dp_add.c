#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <stdbool.h>
#include <string.h>

#define SIZE 1000
#define ITERATIONS 100000

#define MIN( a, b)  ((a)<(b)? (a): (b))

typedef struct threadArgs threadArgs_t;

struct threadArgs {
  pthread_t tid;
  int from;
  int to;
};

double a[SIZE];
double b[SIZE];
double compare[SIZE];

pthread_barrier_t barrier;

/*
 *  Get elapsed time.
 */
int64_t xelapsed (struct timespec a, struct timespec b)
{
	return ((int64_t)a.tv_sec - b.tv_sec) * 1000000
		+ ((int64_t)a.tv_nsec - b.tv_nsec) / 1000LL;
}
/*
 * Here goes the sequential version, very trivial, not much to say about it.
 */
void sequential()
{
	// just two simple nested loops.
	int i, j;
	for (j = 0; j<ITERATIONS; j++) {
		for (i = 0; i < SIZE; i++) {
			a[i] = a[i] + b[i]; 	
		}
	}
}
// the standard simd version - Don't do any iterations
void * simd( void *arg)
{
	int tid,from,to,i;
	// get the information from the struct
	tid = ((threadArgs_t *)arg)->tid;
	from = ((threadArgs_t *)arg)->from;
	to = ((threadArgs_t *)arg)->to;
	/* 
	* Start from "from" and end at "to"
	*/
	for (i = from; i < to; i++) {
		a[i] = a[i] + b[i]; 	
	}
	// return some number, we don't really need it
	return 0;
}
// in  this version we do all the iterations inside the thread
void * simdCheat( void *arg )
{
	int tid,from,to,i,j;
	// get the information from the struct
	tid = ((threadArgs_t *)arg)->tid;
	from = ((threadArgs_t *)arg)->from;
	to = ((threadArgs_t *)arg)->to;
	/* 
	* two nested loop just as in the sequential version
	* however each thread is limited to a range that is has to do work on
	*/
	for (j = 0; j<ITERATIONS; j++) {
		for (i = from; i < to; i++) {
			a[i] = a[i] + b[i]; 	
		}
	}
	// we don't actually need this
	return 0;
}

// in  this version we do all the iterations inside the thread
void * simdCheatWithBarrier( void *arg )
{
	int tid,from,to,i,j;
	// get information from the struct
	tid = ((threadArgs_t *)arg)->tid;
	from = ((threadArgs_t *)arg)->from;
	to = ((threadArgs_t *)arg)->to;
	/* 
	* two nested loop just as in the sequential version
	* however each thread is limited to a range that is has to do work on
	*/
	for (j = 0; j<ITERATIONS; j++) {
		for (i = from; i < to; i++) {
			a[i] = a[i] + b[i]; 	
		}
		// wait for other threads to finish
		pthread_barrier_wait( &barrier );
	}
	// we don't actually need this
	return 0;
}

// in this function we do the addition without barriers
void withoutBarriers(int numThreads, bool cheat){
	
	// malloc the threads
	threadArgs_t ** threads = malloc(numThreads*sizeof(threadArgs_t));
	// parameters
	int t, i, j, rc;
	int increment = SIZE/numThreads;
	int actualIterations;
	// if cheat is true we only want to do one iteration here
	// as the iterations are done in the threads themselves
	if (cheat == true) {
		actualIterations = 1;
	} 
	// if cheat is not true we have the iterations here and join the threads after creating n*iterations threads in total
	else {
		actualIterations = ITERATIONS;
	}
	// do the first loop (just done once if cheat is enabled oterwise ITERATIONS times)
	for (j = 0; j<actualIterations; j++) {
		// create numThread number of threads
		for(t = 0; t < numThreads; t++) {
			// malloc the struct
			threads[t] = malloc(sizeof(struct threadArgs));
			// set from
			threads[t]->from = t*increment;
			// make sure we don't miss any thing due to a uneven number of threads
			// this "hack" might mean that the last thread might have a tiny bit more work than the rest of the threads
			if (t == numThreads-1) {
				threads[t]->to = SIZE-1;
			}
			else {
				threads[t]->to = threads[t]->from+increment;
			}
			// if cheat is false we call the normal simd function
			// otherwise we call the simdCheat function
			if (cheat == false) {
				rc = pthread_create(&threads[t]->tid, NULL, simd, (void *)threads[t]);
			} 
			else {
				rc = pthread_create(&threads[t]->tid, NULL, simdCheat, (void *)threads[t]);
			}

			// catch any errors with thread creations
			if (rc){
				printf("ERROR; return code from pthread_create() is %d\n", rc);
				exit(EXIT_FAILURE);
			}
		}
	
		// join all the threads - very expensive
		for (i = 0; i < numThreads; i++) {
			pthread_join( threads[i]->tid, NULL);
			// make sure we don't leak any memory
			free(threads[t]);
		}
	}
	// make sure we don't leak any memory
	free(threads);
}

void barriers(int numThreads){	
	// malloc the threads
	threadArgs_t ** threads = malloc(numThreads*sizeof(threadArgs_t));
	// parameters
	int i, t, rc;
	int increment = SIZE/numThreads;
	pthread_barrier_init( &barrier, NULL, numThreads);
	
	// creat numThread number of threads
	for(t = 0; t < numThreads; t++) {
		// malloc the struct
		threads[t] = malloc(sizeof(struct threadArgs));
		// set from
		threads[t]->from = t*increment;
		// make sure we don't miss any thing due to a uneven number of threads
		// this "hack" might mean that the last thread might have a tiny bit more work than the rest of the threads
		if (t == numThreads-1) {
			threads[t]->to = SIZE-1;
		}
		else {
			threads[t]->to = threads[t]->from+increment;
		}
		// if cheat is false we call the normal simd function
		// otherwise we call the simdCheat function
		rc = pthread_create(&threads[t]->tid, NULL, simdCheatWithBarrier, (void *)threads[t]);

		// catch any errors with thread creations
		if (rc){
			printf("ERROR; return code from pthread_create() is %d\n", rc);
			exit(EXIT_FAILURE);
		}
	}
	
	// join all the threads - very expensive
	for (i = 0; i < numThreads; i++) {
		pthread_join( threads[i]->tid, NULL);
		// make sure we don't leak any memory
		free(threads[t]);
	}
	// make sure we don't leak any memory
	free(threads);
}

// initialize the data
void initData( )
{
  int i;
  for( i=0; i<SIZE; i++) {
    a[i] = i;
    b[i] = (SIZE-1) - i;
  }
}
// check if the data produced is the same as the sequential version
void checkData(char * type) {
	if (memcmp(compare, a, SIZE) != 0) {
		printf("Error, data for %s is inconsistent! \n", type);
	}
}

int main (int argc, char *argv[])
{
	// make sure the correct arguments are suplied
	if (argc != 2) {
		printf("You must supply a thread number as argument \n");
		exit(1);	
	}
	// make sure we have a postive number of threads bigger than 0
	int numThreads = atoi(argv[1]);
	if (numThreads <= 0 ) {
		printf("The commandline argument must be an integer above 0 \n");
		exit(1);	
	}

	struct timespec start, finish;
	// sequential:
	initData(); // init data first, just to make sure we don't overflow at later points. Should be done before bechmarking
	clock_gettime(CLOCK_REALTIME, &start); // start timing
	sequential(); // call appropiate function
	clock_gettime(CLOCK_REALTIME, &finish); // stop timing
	memcpy (compare, a, SIZE); // copy the array to a new array to compare that all versions get the same results
	printf ("Sequential time: %ld\n",xelapsed (finish, start)); // print result
	checkData("sequential"); // make sure that data is correct
	// with join after each iteraction
	initData();
	bool cheat = false;
	clock_gettime(CLOCK_REALTIME, &start);
	withoutBarriers(numThreads, cheat);
	clock_gettime(CLOCK_REALTIME, &finish);
	printf ("Join after each iteration time: %ld\n",xelapsed (finish, start));
	checkData("join after each");
	// "cheat": do the iterations inside the simd method
	initData();
	cheat = true;
	clock_gettime(CLOCK_REALTIME, &start);
	withoutBarriers(numThreads, cheat);
	clock_gettime(CLOCK_REALTIME, &finish);
	printf ("Iteration inside theads time: %ld\n",xelapsed (finish, start));
	checkData("cheat");
	// With barriers 
	initData();
	clock_gettime(CLOCK_REALTIME, &start);
	barriers(numThreads);
	clock_gettime(CLOCK_REALTIME, &finish);
	printf ("Barriers time: %ld\n",xelapsed (finish, start));
	checkData("barrier");

  	return(0);
}

  

