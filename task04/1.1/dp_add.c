#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

#define N_THREADS 16
#define SIZE 1000
#define ITERATIONS 100000

#define MIN( a, b)  ((a)<(b)? (a): (b))

typedef struct threadArgs threadArgs_t;

struct threadArgs {
  int tid;
  int from;
  int to;
};

double a[SIZE];
double b[SIZE];

pthread_barrier_t barrier;

void printArray(double a[]) {
	int i;
	for (i = 0; i < SIZE; i++) {
		printf("[%d]->%f, ", i, a[i]);	
	}
	printf("\n");

}

void sequential( )
{
  /*
   * Here goes the sequential version
   */
	
	int i, j;
	for (j = 0; j<ITERATIONS; j++)
	{
		for (i = 0; i < SIZE; i++) {
			a[i] = a[i] + b[i]; 	
		}
	}
}

void initData( )
{
  int i;
  for( i=0; i<SIZE; i++) {
    a[i] = i;
    b[i] = (SIZE-1) - i;
  }
  pthread_barrier_init( &barrier, NULL, N_THREADS);
}

int main()
{

  /*
   * Here you need to create your threads run them end sync them 
   */
	initData();
	sequential();

  return(0);
}

  

