#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <time.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <unistd.h>
#include <pthread.h>
#include "mm.h"

// Amount of virtual memory to extend to during initialization... A gigabyte.
//#define GB ( (intptr_t) 1073741824)
//#define GB 3300000
// Huh, apparently we get a plain segfault if this is 3300000, or malloc-tester-01: malloc failed: Success if less than that. Why?!
//#define GB 3299999
#define GB 1073741824
// Data structure (linked list) to store list of "chunks" of memory
typedef struct LIST {size_t size; bool used; struct LIST * prev; struct LIST * next;} list;
int list_size = sizeof(list);
// Start and end of allocatable range
list * start;
void * end;
// Boolean stating whether we have already initialized (extended the heap to 1GB)
bool init_complete = false;
// init_heap simply extends the heap to an extra Gigabyte, returning 
void init_heap() {
	// Extend heap by 1 Gigabyte. Get start pointer of extended range.
	start = (list *) sbrk(GB);
	// Get end pointer of heap, by "extending" it by nothing.
	end = sbrk(0);
	// Set size of first chunk to 1 Gigabyte minus the size of the properties of the list - basically all usable memory 
	start->size = (size_t) GB - list_size;
	// Set used to FALSE to enable allocation of this chunk
	start->used = false;
	// No other chunks exist
	start->prev = NULL;
	start->next = NULL;
	// Ensure this only happens once
	init_complete = true;
}

// Finds the first chunk in the heap which is large enough for the desired allocation and not currently used 
list * find_empty_chunk(size_t size) {
	// Store start point of current chunk - start at start of heap
	list * current = start;
	// If current chunk is not used, and size is greater than minimum size of requested empty chunk
	// While there is a "next" chunk, get next chunk and return it if large enough 
	do {
		if (current->used == false && current->size >= size ) {
			// Return pointer to this chunk
			return current;
		}
		// Get next chunk
		current = current->next;
	} while(current != NULL);
	// Failed to find a large enough chunk, return null
	return NULL;
}

// Return chunk of memory of size requested
void * my_malloc(size_t size){
	// Extend//#define GB ( (intptr_t) 1073741824) the heap if we haven't already
	if (init_complete == 0) {
		init_heap();
	}
	// Get a free chunk
	list * empty_chunk = find_empty_chunk(size);
	// If we failed to get a chunk, return null
	if (empty_chunk == NULL) {
		return (void *) NULL;
	}
	// Get initial size of empty chunk
	size_t empty_chunk_size_old = empty_chunk->size;
	// Set chunk to new size to fit requested size 
	empty_chunk->size = size;
	// Set chunk to being currently used
	empty_chunk->used = true;
	// Get heap position of end of this empty chunk, plus 1 since we are using this to start the next chunk
	//int empty_chunk_end = (intptr_t) empty_chunk + size + 1;
	// Create metadata for next chunk, starting just after end of empty chunk
	list * next = (list *)((size_t)empty_chunk + size + list_size);
	// Create the "next" chunk if: 
	// 	There is more than 0 memory left over after chunk is set to requested size, and;
	// 	Start position of next chunk (after metadata) isn't past the end of the heap
	if ( 	empty_chunk_size_old - size - list_size > 0 && 
			((intptr_t)next + list_size) <= (intptr_t)end ) {
		// Create metadata for next chunk, starting just after end of empty chunk
		//list * next = empty_chunk_end;
		// Set size of next chunk to original size of empty chunk minus part we just allocated, minus size of metadata
		next->size = empty_chunk_size_old - size - list_size;
		// Set used to FALSE to show the next chunk is free, and prev to pointer to start of empty chunk we just created
		next->used = false;
		next->prev = empty_chunk;
		// If the empty chunk we are working with was *not* the exact correct size already, 
		// we will have shrunk the empty chunk and created a new one. 
		// Therefore, we need to preserve any chunk which might have come after the empty chunk. 
		// Thus, we set the "next" value of the chunk we just created to the chunk which came after the empty chunk, 
		// before overwriting the empty chunk's "next" value and setting it to the new chunk.  
		if (empty_chunk->next != next) {
			next->next = empty_chunk->next;
		}
		// Set the empty chunk's next value to the newly created chunk
		empty_chunk->next = next;
	}
	// Return start point of chunk (minus the linked list metadata, since only malloc needs to know about that) 
	return (void *) (empty_chunk + 1);
}

// Negate boolean value stating whether heap chunk is in use, effectively freeing that chunk for reallocation by malloc
void my_free(void *ptr){
	// Create variable to access properties of chunk we wish to clear. Minus 1 to get to the actual list.
	list * chunk_to_clear = ((list *) ptr)-1;
	// Set used property of chunk to FALSE to declare it free and fair game to use for other allocation
	chunk_to_clear->used = false;
}

// For debugging / readable output, loop through the extended heap and print chunk information to stdout
void print_heap() {
	// Start at the start of the heap
	list * current = start;
	// Print start and end information before looping through the chunks
	printf("start: %p end: %p\n", (void *) start, (void *) end);
	// Counter, to keep track of chunks
	int pos = 0;
	// Print current chunk info, if there's a "next", proceed to that and increment counter
	do {
		printf("List at pos %d: current addr: %p size: %zu, used: %d, prev: %p, next %p \n", pos, (void *) current, current->size, current->used, (void *) current->prev, (void *) current->next);
		current = current->next;
		pos++;
	} while (current != NULL);
}
