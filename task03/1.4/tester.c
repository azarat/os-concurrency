#include "mm.h"
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>

int main () {
	int i;
	void * current;
	void * ranCurrent;
	void * array[11000];
	// test 1 allocate a lot of chunks increasing the size each time
	/*for (i=0; i<20; i++) {
		current = my_malloc(100*i);
		array[i] = current;
		if (current == NULL) {break;} // break on errors
	}

	// free the chunks.
	for (i=0; i<20; i++) {
		my_free(array[i]);	
	}
	print_heap();
	sleep(10); // to fetch output*/
	// test 2 interleave big and small chunks
	for (i=0; i<20; i++) {
		if (i%2 == 0)
			current = my_malloc(100);
		else
			current = my_malloc(10000);
		array[i] = current;
		if (current == NULL) {break;} // break on errors
	}
	print_heap();
	// free the chunks.
	for (i=0; i<20; i++) {
		my_free(array[i]);	
	}

}
