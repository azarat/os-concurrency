#include <stdlib.h>

extern void set_debug();
void print_heap();
extern void * my_malloc(size_t size);
extern void my_free(void *ptr);
