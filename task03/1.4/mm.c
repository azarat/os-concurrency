#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <time.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <unistd.h>
#include <pthread.h>
#include "mm.h"

// Amount of virtual memory to extend to during initialization... A gigabyte.
#define GB 1073741824
#define BUCKET_TRESHOLD 256
#define BUCKET_SIZE 16384*2*2*2 // big big bucket size makes malloc-tester-03 complete!
// Data structure (linked list) to store list of "chunks" of memory
typedef struct LIST {size_t size; bool used; struct LIST * prev; struct LIST * next; struct LIST * prev_free; struct LIST * next_free; struct BUCKET * inbucket;} list;
// Date structure for a bucket to hold smaller chunks
typedef struct BUCKET {list * start; void * end; list * first_free; struct BUCKET * prev; struct BUCKET * next;} bucket;
int list_size = sizeof(list);
// Start and end of allocatable range
list * start;
void * end;
list * first_free;
// bucket parameters
bucket * start_bucket;
bucket * bucket_in_use;
// Boolean stating whether we have already initialized (extended the heap to 1GB)
bool init_complete = false;
bool debug = false;
// used for debug.
void set_debug() {
	debug = true;
}
void puts_debug(char * toprint) {
	if (debug == true) {
		puts(toprint);
	}
}

// init_heap simply extends the heap to an extra Gigabyte, returning 
void init_heap() {
	// Extend heap by 1 Gigabyte. Get start pointer of extended range.
	start = (list *) sbrk(GB);
	first_free = start;
	// Get end pointer of heap, by "extending" it by nothing.
	end = sbrk(0);
	// Set size of first chunk to 1 Gigabyte minus the size of the properties of the list - basically all usable memory 
	start->size = (size_t) GB - list_size;
	// Set used to FALSE to enable allocation of this chunk
	start->used = false;
	// No other chunks exist
	start->prev = NULL;
	start->next = NULL;
	start->prev_free = NULL;
	start->next_free = NULL;
	start->inbucket = NULL;
	// Ensure this only happens once
	init_complete = true;
	// print our heap
	printf("start: %p end: %p sizeof(list): %d \n", (void *) start, (void *) end, list_size);
}
// creates an new bucket a bucket have the same internal implementation as this malloc does, but it only contains
// mallocs less than 256. This helps fragmentation as smaller allocations are grouped together
bucket * create_new_bucket(bucket * last_bucket) {
	// get a new bucket, lets use our malloc function for it
	bucket * new_bucket = my_malloc(BUCKET_SIZE);
	// make sure it's not null
	if (new_bucket == NULL) {
		return NULL;
	}
	// set the start and end of the bucket
	new_bucket->start = (list *)((size_t)new_bucket+sizeof(bucket));
	new_bucket->end = (void *)((size_t)new_bucket+BUCKET_SIZE);
	// set all the details about the bucket
	new_bucket->start->size = BUCKET_SIZE-(size_t)list_size;
	new_bucket->start->used = false;
	new_bucket->start->prev = NULL;
	new_bucket->start->next = NULL;
	new_bucket->start->prev_free = NULL;
	new_bucket->start->next_free = NULL;
	new_bucket->start->inbucket = new_bucket;
	new_bucket->first_free = new_bucket->start;
	// set it in the list correctly
	if (last_bucket == NULL) {
		start_bucket = new_bucket;
		new_bucket->prev = NULL;
	}
	else {
		last_bucket->next = new_bucket;
		new_bucket->prev = last_bucket;
	}
	return new_bucket;
}

// Finds the first chunk in the heap which is large enough for the desired allocation and not currently used 
list * find_empty_chunk(size_t size) {
	list * current;
	// if size is smaller than the bucket threshold we need to put it in a bucket
	if (size < BUCKET_TRESHOLD) {
		// if no buckets exsists, create one
		if (start_bucket == NULL) {
			create_new_bucket(NULL);
			current = start_bucket->first_free;
		}
		// loop over all of our buckets
		bucket * current_bucket = start_bucket;
		do {
			// loop over all the values in our bucket
			current = current_bucket->first_free;
			do {
				// if a chunk is found we return it
				if (current->used == false && current->size >= size ) {
					// Return pointer to this chunk
					bucket_in_use = current_bucket;
					return current;
				}
				// Get next chunk
				current = current->next_free;
				//current = current->next;
			} while(current != NULL);
			// if we didn't find a space in the previous bucket, create a new one
			if (current_bucket->next == NULL) {
				if ((intptr_t)current_bucket+sizeof(bucket)+sizeof(list)+(intptr_t)BUCKET_SIZE < (intptr_t)end) {
					create_new_bucket(current_bucket);
				}
			}
			// get the next bucket
			current_bucket = current_bucket->next;
			
		} while (current_bucket != NULL);
	}
	// if value is bigger than bucket_treshold malloc as you normally do
	else {
		// Store start point of current chunk - start at start of heap
		current = first_free;

		// If current chunk is not used, and size is greater than minimum size of requested empty chunk
		// While there is a "next" chunk, get next chunk and return it if large enough 
		do {
			if (current->used == false && current->size >= size ) {
				// Return pointer to this chunk
				return current;
			}
			// Get next chunk 6252 jeb14     20   0 1033m 827m  448 R 99.7 22.0  69:23.18 malloc-tester-0                                                                                                       

			current = current->next_free;
			//current = current->next;
		} while(current != NULL);
	}

	// Failed to find a large enough chunk, return null
	return NULL;
}

// Return chunk of memory of size requested
void * my_malloc(size_t size){
	// Extend//#define GB ( (intptr_t) 1073741824) the heap if we haven't already
	puts_debug("malloc start");
	if (init_complete == 0) {
		init_heap();
	}
	// Get a free chunk
	list * empty_chunk = find_empty_chunk(size);
	// If we failed to get a chunk, return null
	if (empty_chunk == NULL) {
		return (void *) NULL;
	}
	
	// Get initial size of empty chunk
	size_t empty_chunk_size_old = empty_chunk->size;
	// Set chunk to new size to fit requested size 
	empty_chunk->size = size;
	// Set chunk to being currently used
	empty_chunk->used = true;

	// Get heap position of end of this empty chunk, plus 1 since we are using this to start the next chunk
	//int empty_chunk_end = (intptr_t) empty_chunk + size + 1;
	// Create metadata for next chunk, starting just after end of empty chunk
	list * next = (list *)((size_t)empty_chunk + size + list_size);
	// we need to make sure we have the right memory "end", i.e if it's a bucket it should be the buckets end
	// otherwise the end of our whole memory space
	intptr_t appropriate_end;
	if (size < BUCKET_TRESHOLD) {
		appropriate_end = (intptr_t)bucket_in_use->end;
		// not quite sure why this would ever be the case :( 
		if (appropriate_end>(intptr_t)end)
		{
			appropriate_end = (intptr_t)end;
		}
	}
	else
	{
		appropriate_end = (intptr_t)end;
	}
	// Create the "next" chunk if: 
	// 	There is more than 0 memory left over after chunk is set to requested size, and;
	// 	Start position of next chunk (after metadata) isn't past the end of the heap
	if ( 	empty_chunk_size_old - size - list_size > 0 && 
			((intptr_t)next + list_size) <= appropriate_end) { 
		// Create metadata for next chunk, starting just after end of empty chunk
		//list * next = empty_chunk_end;
		// Set size of next chunk to original size of empty chunk minus part we just allocated, minus size of metadata
		next->size = empty_chunk_size_old - size - list_size;
		// Set used to FALSE to show the next chunk is free, and prev to pointer to start of empty chunk we just created
		next->used = false;
		next->prev = empty_chunk;

		// If the empty chunk we are working with was *not* the exact correct size already, 
		// we will have shrunk the empty chunk and created a new one. 
		// Therefore, we need to preserve any chunk which might have come after the empty chunk. 
		// Thus, we set the "next" value of the chunk we just created to the chunk which came after the empty chunk, 
		// before overwriting the empty chunk's "next" value and setting it to the new chunk.  
		if (empty_chunk->next != next) {
			if ((intptr_t)empty_chunk->next < (intptr_t)appropriate_end &&
			    (intptr_t)empty_chunk->next < (intptr_t) end) 
				next->next = empty_chunk->next;
			else 
				next->next = NULL;
		}
		// Set the empty chunk's next value to the newly created chunk
		empty_chunk->next = next;
		next->prev_free = empty_chunk->prev_free;
		next->next_free = empty_chunk->next_free;
		// if we're in a bucket we should set the first free and inbucket value as appropiate
		if (size < BUCKET_TRESHOLD) {
			if (empty_chunk == bucket_in_use->first_free) { 
				bucket_in_use->first_free=next; 
			}
			next->inbucket = bucket_in_use;
		}
		// else set the global first free to next and the inbucket to null
		else
		{
			if (empty_chunk == first_free) { 
				first_free=next; 
			}
			next->inbucket = NULL;
		}
		// update the linked list pointers
		if (empty_chunk->prev_free != NULL ) {
			empty_chunk->prev_free->next_free = next;
		}

		
	} 

	return (void *) (empty_chunk + 1);
}

// null a whole chunk
void null_chunk(list * chunk) {
	chunk->size=0;
	chunk->used=false;
	chunk->prev=NULL;
	chunk->next=NULL;
	chunk->prev_free = NULL; 
	chunk->next_free = NULL;
}                                                                                                    
// No longer used. Finds a bucket if the chunk is in one
bucket * find_in_bucket(list * ptr) {
	if (start_bucket == NULL) { return NULL; }
	bucket * current_bucket = start_bucket;
	do {
		if ((size_t)ptr > (size_t)current_bucket->start && (size_t)ptr < (size_t)current_bucket->end)
		{
			return current_bucket;
		}
		current_bucket = current_bucket->next;
	} while (current_bucket != NULL);
	return NULL;
}

// Negate boolean value stating whether heap chunk is in use, effectively freeing that chunk for reallocation by malloc
// merge any adjacent chunks, relink the list
void my_free(void *ptr){
	// Create variable to access properties of chunk we wish to clear. Minus 1 to get to the actual list.
	list * chunk_to_clear = ((list *) ptr)-1;
	
	//bucket * current_bucket = find_in_bucket(chunk_to_clear);
	bucket * current_bucket = chunk_to_clear->inbucket; // this doesn't seem much faster for some reason, maybe we should just search and keep the overhead? furhter tests needed.
	//  get the first_free we need to used depending on whether we're in a bucket or not
	list * relevant_first_free;
	if (current_bucket == NULL) {
		relevant_first_free = first_free;
	}
	else {
		relevant_first_free = current_bucket->first_free;
	}
	// make sure we actually have valid data here
	if (chunk_to_clear->used == true) {
		// Set used property of chunk to FALSE to declare it free and fair game to use for other allocation
		chunk_to_clear->used = false;
		// set a bool for future use
		bool done_merge = false;
		// a null check to avoid any crashes
		if (chunk_to_clear->prev != NULL) {
			// if the previous chunk is used, if not, lets merge them
			if (chunk_to_clear->prev->used == false) {
				// set the new chunk to be the previous node of the chunk we clear
				list * new = chunk_to_clear->prev;
				// set the new size
				new->size = new->size + list_size + chunk_to_clear->size;				
				// set the proper next
				new->next = chunk_to_clear->next;

				// set the proper previous
				if (new->next != NULL) 
					new->next->prev = new;
				// make sure we make everything null the the chunk that are no no longer used. Security reasons.
				null_chunk(chunk_to_clear);
				// set chunk to clear as new
				chunk_to_clear = new;
				// set boolean
				done_merge = true;
			}
		}
		// Check if next is unused
		if (chunk_to_clear->next != NULL) {
			if (chunk_to_clear->next->used == false) {
				// save the old node
				list * old_node = chunk_to_clear->next;
				// calculate the new size
				chunk_to_clear->size = old_node->size + list_size + chunk_to_clear->size;
				// set the correct next
				chunk_to_clear->next = old_node->next;

				// set the correct next free
				chunk_to_clear->next_free = old_node->next_free;
				// set all in the old node to null
				null_chunk(old_node);
				// set the flag
				done_merge = true;
			}		
		}
		
		// if no merging can be done
		if (done_merge == false) {
			// we set the next_free to be the first_free
			chunk_to_clear->next_free = relevant_first_free;
			// first_free prev_free is this chunk
			relevant_first_free->prev_free = chunk_to_clear;
		}
		// set first_free to this chunk
		if (current_bucket != NULL) {
			current_bucket->first_free = chunk_to_clear;
		}
		else {
			first_free = chunk_to_clear;
		}
		// if we're in a bucket we should make sure it's not empty, if it's empty we should get rid of it.
		if (current_bucket != NULL) {
			// and this bucket only have one element in it
			if (chunk_to_clear->next == NULL && chunk_to_clear->prev == NULL) {
				if (current_bucket == start_bucket) {
					chunk_to_clear->size = 0;
					start_bucket = current_bucket->next;
				}
				else 
				{
					current_bucket->next->prev = current_bucket->prev;
					current_bucket->prev->next = current_bucket->next;
					chunk_to_clear->size = 0;
				}
				// then we should get rid of that bucket
				my_free(current_bucket);
			}
		}
	}	
}
// helper function to print all buckets
void print_bucket(bucket * print) {
		printf("--bucket: %p--\n", (void *) print);
		// Start at the start of the heap
		list * current = print->start;
		// Print start and end information before looping through the chunks
		printf("start: %p end: %p\n", (void *) print->start, (void *) print->end);
		// print first free:
		printf("First free: %p \n", print->first_free);
		// Counter, to keep track of chunks
		int pos = 0;
		// Print current chunk info, if there's a "next", proceed to that and increment counter
		do {
			printf("List at pos %d: current addr: %p size: %zu, used: %d, prev: %p, next %p prev_free: %p, next_free: %p \n", pos, (void *) current, current->size, current->used, (void *) current->prev, (void *) current->next, (void *) current->prev_free, (void *) current->next_free);
			current = current->next;
			pos++;
		} while (current != NULL);


}

void print_buckets() {
	puts("-----------------BUCKETS---------------------");
	bucket * current_bucket = start_bucket;
	do {
		printf("--bucket: %p--\n", (void *)current_bucket);
		// Start at the start of the heap
		list * current = current_bucket->start;
		// Print start and end information before looping through the chunks
		printf("start: %p end: %p\n", (void *) current_bucket->start, (void *) current_bucket->end);
		// print first free:
		printf("First free: %p \n", current_bucket->first_free);
		// Counter, to keep track of chunks
		int pos = 0;
		// Print current chunk info, if there's a "next", proceed to that and increment counter
		do {
			printf("List at pos %d: current addr: %p size: %zu, used: %d, prev: %p, next %p prev_free: %p, next_free: %p \n", pos, (void *) current, current->size, current->used, (void *) current->prev, (void *) current->next, (void *) current->prev_free, (void *) current->next_free);
			current = current->next;
			pos++;
		} while (current != NULL);
		current_bucket = current_bucket->next;
		
	} while (current_bucket != NULL);

	puts("-----------------BUCKETS---------------------");
}
// For debugging / readable output, loop through the extended heap and print chunk information to stdout
void print_heap() {
	puts("---------------------------------------");
	// Start at the start of the heap
	list * current = start;
	// Print start and end information before looping through the chunks
	printf("start: %p end: %p\n", (void *) start, (void *) end);
	// print first free:
	printf("First free: %p \n", first_free);
	// Counter, to keep track of chunks
	int pos = 0;
	// Print current chunk info, if there's a "next", proceed to that and increment counter
	do {
		printf("List at pos %d: current addr: %p size: %zu, used: %d, prev: %p, next %p prev_free: %p, next_free: %p \n", pos, (void *) current, current->size, current->used, (void *) current->prev, (void *) current->next, (void *) current->prev_free, (void *) current->next_free);
		current = current->next;
		pos++;
	} while (current != NULL);
	puts("---------------------------------------");
	if (start_bucket != NULL) print_buckets();
}
